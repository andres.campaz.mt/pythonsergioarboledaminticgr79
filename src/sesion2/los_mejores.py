""" Programa para ver cuales son mis mejores estudiantes
"""

resultados_estudiantes = {"jose": 10, "manuel": 2, "camilo": 7, "paula": 9, "viviana": 10,
                          "andrea": 8, "laura": 7, "maria": 6, "ana": 8, "carlos": 5, "luis": 6}

los_mejores = []
los_que_hay_que_motivar = []

# Para cada estudiante con resultado
for estudiante, resultado in resultados_estudiantes.items():

    # Si el resultado es mayor o igual que 9
    if resultado >= 9:
        # Agregar a los mejore el estudiante
        los_mejores.append(estudiante)
        print(f'felicitaciones {estudiante}')
    # Si no
    else:
        # Agregar a los que hay que motivar
        los_que_hay_que_motivar.append(estudiante)
        print(f'{estudiante} siempre destacandose por la vagancia')

print(f'Felicitaciones a Los mejores estudiantes {los_mejores}')
print(
    f'Los decepcionantes deben trabajar mas {los_que_hay_que_motivar}... Estudien Vagos!')
